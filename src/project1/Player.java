/**
 * Player.java
 * CS 342 - Project 1
 */
package project1;
/**
 * Player class defines the attributes of a player, including the name, cardHand and the Rank of the hand
 * @author Aditya Jain, Anthony Colon
 */
public class Player {
	private CardPile cardHand = new CardPile();
	private HandRank evalHand = null;
	private String name;
	
	/**
	 * This enum is used to store all possible hand ranks in order from lowest 
	 * to highest.
	 */
	public static enum HandRank {
		HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND, STRAIGHT, FLUSH,
		FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH
	}
	
	/**
	 * Sets the name of player upon creation of instance
	 * @param name
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * Checks whether a given Hand has an Ace
	 * @return true if there is an Ace, and false otherwise
	 */
	public boolean hasAce() {
		return this.cardHand.contains(Card.Rank.ACE);
	}
	
	/**
	 * The player's hand printed neatly as a string with index values
	 */
	public String toString() {
		String outputString = new String();
		for (int i=0; i<cardHand.size(); i++) {
			if(cardHand.getCard(i).getCardRank().ordinal()==8){
				outputString += "[" + (i+1) +"] " + cardHand.getCard(i).toString() + "  ";
			}
			else{	
				outputString += "[" + (i+1) +"] " + cardHand.getCard(i).toString() + "   ";
			}
		}
		return outputString;
	}
	
	/**
	 * Evaluates the hand if it hasn't already been done and returns the result.
	 * @return Rank of the hand in an ordinal number
	 */
	public HandRank getHandRank() {
		if(evalHand==null){
			 evaluateHand();
		}
		return evalHand;
	}
	

	/**
	 * Returns the HandRank of the player it is called upon as a string
	 * @return HandRank of the Player as a string
	 */
	public String printHandRank() {
		if(evalHand==null)
		{
			this.evaluateHand();
		}
		return evalHand.toString().replace("_", " ");
	}

	/**
	 * Evaluates the hand by checking conditions in order from highest hand
	 * to lowest hand.
	 */
	public void evaluateHand() {
		if (cardHand.isSameSuit() && cardHand.isSequence())
			evalHand = HandRank.STRAIGHT_FLUSH;
		else if (cardHand.getCard(0).getMatchCount() == 3)
			evalHand = HandRank.FOUR_OF_A_KIND;
		else if (cardHand.getCard(0).getMatchCount() == 2
				&& cardHand.getCard(3).getMatchCount() == 1)
			evalHand = HandRank.FULL_HOUSE;
		else if (cardHand.isSameSuit())
			evalHand = HandRank.FLUSH;
		else if (cardHand.isSequence())
			evalHand = HandRank.STRAIGHT;
		else if (cardHand.getCard(0).getMatchCount() == 2)
			evalHand = HandRank.THREE_OF_A_KIND;
		else if (cardHand.getCard(0).getMatchCount() == 1 &&
				cardHand.getCard(2).getMatchCount() == 1)
			evalHand = HandRank.TWO_PAIR;
		else if (cardHand.getCard(0).getMatchCount() == 1)
			evalHand = HandRank.ONE_PAIR;
		else
			evalHand = HandRank.HIGH_CARD;
	}

	/**
	 * Discards a card from the Deck it is called upon and draws a card
	 * @param cardIndex Index of the card to be removed
	 * @param sourceDeck Deck from which to draw a card
	 * @param discardPile Deck to which cards will be discarded to
	 * @param amount The amount of cards to discard and draw
	 */
	public void discardAndDrawCard(int cardIndex, CardPile sourceDeck, CardPile discardPile, int amount) {
		for (int i=0; i<amount; i++)
			cardHand.discard(cardIndex, discardPile);
		for (int i=0; i<amount; i++)
			cardHand.draw(sourceDeck);
	}

	/**
	 * @return the cardHand
	 */
	public CardPile getCardHand() {
		return cardHand;
	}
	
	/**
	 * Wrapper function to sort the given cardHand
	 */
	public void sort()
	{
		cardHand.sort();
	}
	
	/**
	 * Compares two players by their hands.
	 * @param otherPlayer Player to compare against
	 * @return Result of comparison
	 */
	public int compareTo(Player otherPlayer) {
		int comparison = this.getHandRank().compareTo(otherPlayer.getHandRank());
		if (comparison != 0) {
			return comparison;
		} else {
			//a tie
			switch(evalHand) {
			case STRAIGHT_FLUSH:
			case FOUR_OF_A_KIND:
			case FULL_HOUSE:
			case FLUSH:
			case STRAIGHT:
			case THREE_OF_A_KIND:
				return this.getCardHand().getCard(0).compareTo(otherPlayer.getCardHand().getCard(0));	//the above hands have the same rule for breaking ties
			case TWO_PAIR:
				int pairCompare1 = this.getCardHand().getCard(0).compareTo(otherPlayer.getCardHand().getCard(0));
				if (pairCompare1 != 0) {
					return pairCompare1;	
				} else {
					int pairCompare2 = this.getCardHand().getCard(2).compareTo(otherPlayer.getCardHand().getCard(2));
					return pairCompare2;
				}
			case ONE_PAIR:
				int pairCompare = this.getCardHand().getCard(0).compareTo(otherPlayer.getCardHand().getCard(0));
				if (pairCompare != 0) {
					return pairCompare;
				} else {
					for (int i=2; i<Game.HAND_SIZE; i++) {
						int cardCompare = this.getCardHand().getCard(i).compareTo(otherPlayer.getCardHand().getCard(i));
						if (cardCompare !=0) return cardCompare;
					}
					return 0;	//made it out of for loop, then it is a tie.
				}
			case HIGH_CARD:
				for (int i=0; i<Game.HAND_SIZE; i++) {
					int cardCompare = this.getCardHand().getCard(i).compareTo(otherPlayer.getCardHand().getCard(i));
					if (cardCompare !=0) return cardCompare;
				}
				return 0;	//made out of for loop, then it is a tie.
			}
		}
		return 0;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
