
/**
 * Game.java
 * CS 342 - Project 1
 */
package project1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Controls the flow of the program including Setting up the game, Deal phase, Discard and Draw phase 
 * and the End Result phase 
 * @author Aditya Jain, Anthony Colon	
 */
public class Game {
	public static final int HAND_SIZE = 5;
	private int numAI = -1;
	private CardPile deck = new CardPile();
	private CardPile discardPile = new CardPile();
	private UserPlayer user;
	private AIPlayer[] AIPlayers;
	
	public Game() {
		System.out.println("- Poker Game -\nAnthony Colon and Aditya Jain");
		System.out.println("UIC CS 342 Spring 2014 Project 1");
	}
	
	public static void main(String[] args) {		
		Game PokerGame = new Game();
		Scanner scan = new Scanner(System.in);
		PokerGame.setupPhase(scan);	
		PokerGame.discardAndDrawPhase(scan);
		PokerGame.endPhase();
		scan.close();
	}

	/**
	 * Game end phase. Winner determined and displayed
	 */
	private void endPhase() {
		List<Player> players = new ArrayList<Player>();
		
		System.out.println("\n- Game End Phase -");
		System.out.println("Your hand: " + user + user.printHandRank());
		players.add(user);
		
		for(int i=0;i<numAI;i++)
		{
			System.out.println(AIPlayers[i].getName() + " hand: " + AIPlayers[i] + AIPlayers[i].printHandRank());
			players.add(AIPlayers[i]);
		}
		
		Collections.sort(players, new PlayerCompareDesc());
		int standing = 1;
		System.out.println("\nResults: ");
		for (int i=0; i<numAI+1; i++) {
			System.out.println((standing) + ". " + players.get(i).getName() + "  - " + players.get(i) + players.get(i).printHandRank() );
			if (i!=numAI && players.get(i).compareTo(players.get(i+1)) != 0) standing++;	//standing incremented only if there is not a tie
		}
		
		if (players.get(0).compareTo(players.get(1)) == 0) {
			System.out.println("\nThere is a tie.");
		} else {
			System.out.println("\nWinner: " + players.get(0).getName());
		}
		
	}

	private void discardAndDrawPhase(Scanner scan) {
		System.out.println("\n- Discard and draw phase -");
		System.out.println("Your hand: \t" + user);
		user.discardPhase(deck, discardPile, scan);
		for(int i=0;i<numAI;i++)
		{
			int numCardsDiscarded = AIPlayers[i].discardPhase(deck, discardPile);
			System.out.println(AIPlayers[i].getName() +" has discarded "+numCardsDiscarded+" cards");
		}
	}

	/**
	 * Creates the main deck for the game
	 */
	private void createPokerDeck() {
		System.out.println("The deck is being created and shuffled.");
		deck.addDeck();
	}
	
	/**
	 * Deals the cards to the various players and sort hands.
	 */
	private void dealCards() {
		System.out.println("The cards are being dealt to you and "+numAI+" computer player(s)");
		user = new UserPlayer("You ");
		AIPlayers = new AIPlayer[numAI];
		for(int i=0;i<numAI;i++)
		{
			AIPlayers[i] =new AIPlayer("CPU" + (i+1));
		}
		
		//Deal five cards to each player, one at a time
		for (int i=0; i<HAND_SIZE; i++) {
			user.getCardHand().draw(deck);
			for (int j=0; j<numAI; j++) {
				AIPlayers[j].getCardHand().draw(deck);
			}
		}
		
		//sort all hands
		user.sort();
		for (int j=0; j<numAI; j++) {
			AIPlayers[j].sort();
		}
	}

	/**
	 * Sets up the game
	 * @param scan object for input stream
	 */
	private void setupPhase(Scanner scan) {
		System.out.println("\n- Game Setup Phase -");
		System.out.println("Please enter number of computer opponents (1-3)");
		while (true) {
			if (!scan.hasNextInt()) {
				scan.next();
			} else {
				numAI = scan.nextInt();
				if (numAI >= 1 && numAI <= 3)
					break; // valid input
			}
			System.err.println("Please enter a number from 1 to 3");
		}
		this.createPokerDeck();
		this.dealCards();
	}
	
	/**
	 * PlayerCompareDesc class provides the comparator for sorting players by hand
	 * in descending order.
	 */
	private class PlayerCompareDesc implements Comparator<Player> {
		/**
		 * Compares two players
		 */
	    @Override
	     public int compare(Player player1, Player player2) {
	        return player2.compareTo(player1);
	    }
	}
}
