/**
 * AIPlayer.java
 * CS 342 Project 1 - Poker Game
 */

package project1;

/**
 * This class is inherited from Player class. It is used for
 * the behavior of the computer player.
 * 
 * @author Anthony Colon, Aditya Jain
 */
public class AIPlayer extends Player{
	
	/**
	 * Sets name of AI player, inherited from Player
	 * @param name name of AI
	 */
	public AIPlayer(String name) {
		super(name);	//constructor inherited from Player.java
	}

	/**
	 * Algorithm for AI players to follow for the discarding phase
	 * @param sourceDeck Deck from which cards will be drawn
	 * @param discardPile Card pile to store discarded cards
	 */
	public int discardPhase(CardPile sourceDeck, CardPile discardPile) {
		if(this.getHandRank()==HandRank.HIGH_CARD){	//if it is a High Card
			int fourCardsSameSuitIndex=this.fourCardsSameSuite();
			if(fourCardsSameSuitIndex!=-1){	//discard 5th non-same suit card
				this.discardAndDrawCard(fourCardsSameSuitIndex,sourceDeck, discardPile, 1); 
				this.sort();
				evaluateHand();
				return 1;
			}
			else{
				int fourCardsSameSequenceIndex=this.fourCardsSameSequence();
				if(fourCardsSameSequenceIndex!=-1){	//discard 5th non-same sequence card	
					this.discardAndDrawCard(fourCardsSameSequenceIndex,sourceDeck, discardPile, 1);
					this.sort();
					evaluateHand();
					return 1;
				}
				else{
					if(this.hasAce())
					{	//discard all cards that are not the Ace
						this.discardAndDrawCard(1,sourceDeck, discardPile, 4);
						this.sort();
						evaluateHand();
						return 4;
					}
					else{	//discard three lower rank cards
						this.discardAndDrawCard(2,sourceDeck, discardPile, 3);
						this.sort();
						evaluateHand();
						return 3;
					}
				}
			}
		}
		else if(this.getHandRank()==HandRank.ONE_PAIR){	//if hand is a Pair, discard three lower ranked cards
			this.discardAndDrawCard(2,sourceDeck, discardPile, 3);
			this.sort();
			evaluateHand();
			return 3;
		}
		else if(this.getHandRank()==HandRank.TWO_PAIR){	//if two pairs, discard last card
			this.discardAndDrawCard(4,sourceDeck, discardPile, 1);
			this.sort();
			evaluateHand();
			return 1;
		}
		else if(this.getHandRank()==HandRank.THREE_OF_A_KIND){	//if three of a kind, discard two lower cards
			this.discardAndDrawCard(3,sourceDeck, discardPile, 2);
			this.sort();
			evaluateHand();
			return 2;
		}
		return 0;
	}
	
	/**
	 * Checks whether or not four cards within a deck are of the same sequence. 
	 * @return Index of the 5th non-same sequence card 
	 */
	public int fourCardsSameSequence() {
		if((getCardHand().getCard(0).getCardRank().ordinal()==(getCardHand().getCard(1).getCardRank().ordinal()+1))&&
				(getCardHand().getCard(1).getCardRank().ordinal()==(getCardHand().getCard(2).getCardRank().ordinal()+1))&&
				(getCardHand().getCard(2).getCardRank().ordinal()==(getCardHand().getCard(3).getCardRank().ordinal()+1)))
			{
				return 4;
			}
			else if((getCardHand().getCard(1).getCardRank().ordinal()==(getCardHand().getCard(2).getCardRank().ordinal()+1))&&
					(getCardHand().getCard(2).getCardRank().ordinal()==(getCardHand().getCard(3).getCardRank().ordinal()+1))&&
					(getCardHand().getCard(3).getCardRank().ordinal()==(getCardHand().getCard(4).getCardRank().ordinal()+1)))
			{
				return 0;
			}
			return -1;
	}


	/**
	 * Checks whether or not four cards within a deck are of the same suit.
	 * @return Index of the 5th non-same suit card 
	 */
	public int fourCardsSameSuite() {
		if((getCardHand().getCard(0).getCardSuit()==getCardHand().getCard(1).getCardSuit())&&
			(getCardHand().getCard(1).getCardSuit()==getCardHand().getCard(2).getCardSuit())&&
			(getCardHand().getCard(2).getCardSuit()==getCardHand().getCard(3).getCardSuit()))
		{
			return 4;
		}
		else if((getCardHand().getCard(1).getCardSuit()==getCardHand().getCard(2).getCardSuit())&&
				(getCardHand().getCard(2).getCardSuit()==getCardHand().getCard(3).getCardSuit())&&
				(getCardHand().getCard(3).getCardSuit()==getCardHand().getCard(4).getCardSuit()))
		{
			return 0;
		}
		return -1;
	}
}
