/**
 * Card.java
 * CS 342 Project 1 - Poker Game
 */

package project1;

/**
 * This class is used to store information about a card
 * and perform common operations for comparing.
 * 
 * @author Anthony Colon, Aditya Jain
 *
 */
public class Card 
{
	/**
	 * This enum contains all possible suits.
	 * 
	 */
	public static enum Suit {
		CLUB, DIAMOND, HEART, SPADE
	}
	
	/**
	 * This enum contains all possible ranks.
	 * 
	 */
	public static enum Rank {
		TWO, THREE, FOUR, FIVE, SIX, SEVEN,
		EIGHT, NINE, TEN, JACK, QUEEN, KING,
		ACE
	}
	
	private Suit cardSuit;
	private Rank cardRank;
	private int matchCount;
	
	/**
	 * Constructor for creating a card based on
	 * suit and rank.
	 * 
	 * @param cardSuit Suit of card
	 * @param cardRank Rank of card
	 */
	public Card(Suit cardSuit, Rank cardRank) {
		super();
		this.cardSuit = cardSuit;
		this.cardRank = cardRank;
		this.matchCount = 0;
	}

	/**
	 * Compares the suit of current card with another card.
	 * @param otherCard Card to compare to
	 * @return Result of comparison
	 */
	public boolean sameSuit(Card otherCard) {
		if (this.getCardSuit() == otherCard.getCardSuit())
			return true;
		else
			return false;
	}
	
	/**
	 * Compares the rank of current card with another card.
	 * @param otherCard Card to compare to
	 * @return Result of comparison
	 */
	public boolean sameRank(Card otherCard) {
		if (this.getCardRank() == otherCard.getCardRank())
			return true;
		else
			return false;
	}
	
	/**
	 * Compares the current card rank with the rank of another card.
	 * 
	 * @param otherCard Card to compare to
	 * @return the difference between the cards
	 */
	public int compareTo(Card otherCard) {
		return (this.getCardRank().compareTo(otherCard.getCardRank()));
	}

	/**
	 * Getter for card suit.
	 * @return the cardSuit
	 */
	public Suit getCardSuit() {
		return cardSuit;
	}

	/**
	 * Getter for card rank.
	 * @return the cardRank
	 */
	public Rank getCardRank() {
		return cardRank;
	}
	
	/**
	 * The card suit and card rank information
	 */
	public String toString() {
		String outputString = new String();
		int rank = this.getCardRank().ordinal(); 
		switch(rank) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
			outputString += rank + 2;
			break;
		case 9:
			outputString += "J";
			break;
		case 10:
			outputString += "Q";
			break;
		case 11:
			outputString += "K";
			break;
		case 12:
			outputString += "A";
			break;	
		default:
			return null;
		}
		outputString+=this.getCardSuit().toString().charAt(0);
		return outputString;
	}

	/**
	 * Getter for matchCount
	 * @return the matchCount
	 */
	public int getMatchCount() {
		return matchCount;
	}

	/**
	 * Setter for matchCount
	 * @param matchCount the matchCount to set
	 */
	public void setMatchCount(int matchCount) {
		this.matchCount = matchCount;
	}
}
