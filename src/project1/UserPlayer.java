/**
 * UserPlayer.java
 * CS 342 - Project 1
 */
package project1;
/**
 * UserPlayer class is inherited from the Player class and defines the functions used by the Human Player 
 * @author Aditya Jain, Anthony Colon
 */
import java.util.Scanner;

public class UserPlayer extends Player {
	
	/**
	 * Sets name of user, inherited from Player
	 * @param name name of user
	 */
	public UserPlayer(String name) {
		super(name);
	}

	private static final int LOW_INDEX = 0;
	
	/**
	 * Constructor inherited from Player class. Sets up the player and hand.
	 */

	public void discardPhase(CardPile sourceDeck, CardPile discardPile, Scanner scan) {
		int discardCount = 0;
		int discardIndex;
		if (hasAce()){
			System.out.println("Since you have an Ace you can keep the Ace and discard the other four cards.");	
		}
		else{
			System.out.println("You may discard 3 cards.");
		}		
		while (true) {
			System.out.println("Enter the card index to discard, or 0 to stop:");
			if (!scan.hasNextInt()) {
				scan.next();
			} else if (scan.hasNextInt()) {
				discardIndex = scan.nextInt();
				if (discardIndex==0) break;
				discardIndex--;	//internal index starts at 0, not 1
				
				if (discardIndex >= LOW_INDEX && discardIndex <= (this.getCardHand().size()-1)) {
					if  (discardCount==3 && hasAce()) {
						if (this.getCardHand().getCard(discardIndex).getCardRank()==Card.Rank.ACE &&			//can't discard ace
								!(this.getCardHand().getCard(1-discardIndex).getCardRank()==Card.Rank.ACE)) {	//if it is not remaining card
							System.out.println("You cannot discard your Ace!");
							continue;
						}
					}
						
					System.out.println("Discarded " + this.getCardHand().getCard(discardIndex));
					this.getCardHand().discard(discardIndex, discardPile);
					discardCount++;
					System.out.println("Your hand: " + this);
					if (discardCount>=4)
						break;
					else if (discardCount>=3 && !hasAce())
						break;					
				} else {
					System.err.println(" Valid indices are from " + (LOW_INDEX+1) + " to " + this.getCardHand().size() + ".");
				}
			}
		}

		if (discardCount>0) {
			System.out.println(discardCount + " card(s) were discarded.");
			System.out.println("Drawing " + discardCount + " card(s).");
			for (int i=0; i<discardCount; i++) {
				this.getCardHand().draw(sourceDeck);
			}
			this.getCardHand().sort();
			System.out.println("Your hand: " + this);
		} else {
			System.out.println("No cards were discarded.");
		}
		this.evaluateHand();
	}
}
