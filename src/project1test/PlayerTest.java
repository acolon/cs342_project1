/**
 * PlayerTest.java
 * CS 342 Project 1 - Poker Game
 */
package project1test;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import project1.Card;
import project1.CardPile;
import project1.Player;

/**
 * Tests for various Player methods to test certain cases
 * @author Anthony Colon, Aditya Jain
 */
public class PlayerTest {
	
	/**
	 * Test for Evaluate Hand
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testEvaluateHand() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.KING));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.JACK));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.NINE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.SEVEN));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.FIVE));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertEquals(Player.HandRank.HIGH_CARD, testPlayer.getHandRank());
	}
	
	/**
	 * Tests to ensure low ace is in correct position and and checking methods
	 * work properly.
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testLowAce() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.ACE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.FIVE));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.THREE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.FOUR));
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.TWO));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
			
		testPlayer.sort();
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertTrue(testPlayer.getCardHand().isSequence());	
		Assert.assertTrue(testPlayer.hasAce());
		Assert.assertEquals(Player.HandRank.STRAIGHT, testPlayer.getHandRank());
		Assert.assertEquals(testPlayer.getCardHand().getCard(0).getCardRank(), Card.Rank.FIVE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(1).getCardRank(), Card.Rank.FOUR);
		Assert.assertEquals(testPlayer.getCardHand().getCard(2).getCardRank(), Card.Rank.THREE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(3).getCardRank(), Card.Rank.TWO);
		Assert.assertEquals(testPlayer.getCardHand().getCard(4).getCardRank(), Card.Rank.ACE);
	}

	/**
	 * Reflection to get access to cardPile List for testing.
	 * @param deck CardPile to get access to
	 * @return Card List
	 * @throws NoSuchFieldException Reflection issue
	 * @throws IllegalAccessException Reflection issue
	 */
	private List<Card> setCardPilePublic(CardPile deck) throws NoSuchFieldException, IllegalAccessException {
		//Reflection to get private cardPile
		Field f = deck.getClass().getDeclaredField("cardPile");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<Card> cardPileTest = (List<Card>) f.get(deck);
		return cardPileTest;
	}
	
	/**
	 * Tests to ensure high ace is in correct position and and checking methods
	 * work properly.
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testHighAce() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.FOUR));
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.NINE));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.ACE));
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.SEVEN));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.TWO));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
			
		testPlayer.sort();
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertFalse(testPlayer.getCardHand().isSequence());	
		Assert.assertTrue(testPlayer.hasAce());
		Assert.assertEquals(Player.HandRank.HIGH_CARD, testPlayer.getHandRank());
		Assert.assertEquals(testPlayer.getCardHand().getCard(0).getCardRank(), Card.Rank.ACE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(1).getCardRank(), Card.Rank.NINE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(2).getCardRank(), Card.Rank.SEVEN);
		Assert.assertEquals(testPlayer.getCardHand().getCard(3).getCardRank(), Card.Rank.FOUR);
		Assert.assertEquals(testPlayer.getCardHand().getCard(4).getCardRank(), Card.Rank.TWO);
	}
	
	/**
	 * Test to see if Straight Flush with high ace is properly identified.
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testStraightFlushHighAce() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.JACK));
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.QUEEN));
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.ACE));
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.TEN));
		cardPileTest.add(new Card(Card.Suit.DIAMOND, Card.Rank.KING));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
			
		testPlayer.sort();
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertTrue(testPlayer.getCardHand().isSequence());
		Assert.assertTrue(testPlayer.getCardHand().isSameSuit());
		Assert.assertTrue(testPlayer.hasAce());
		Assert.assertEquals(Player.HandRank.STRAIGHT_FLUSH, testPlayer.getHandRank());
		Assert.assertEquals(testPlayer.getCardHand().getCard(0).getCardRank(), Card.Rank.ACE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(1).getCardRank(), Card.Rank.KING);
		Assert.assertEquals(testPlayer.getCardHand().getCard(2).getCardRank(), Card.Rank.QUEEN);
		Assert.assertEquals(testPlayer.getCardHand().getCard(3).getCardRank(), Card.Rank.JACK);
		Assert.assertEquals(testPlayer.getCardHand().getCard(4).getCardRank(), Card.Rank.TEN);
	}
	
	/**
	 * Test to see if Straight Flush with low ace is properly identified.
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testStraightFlushLowAce() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.ACE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.FIVE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.THREE));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.FOUR));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.TWO));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
			
		testPlayer.sort();
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertTrue(testPlayer.getCardHand().isSequence());
		Assert.assertTrue(testPlayer.getCardHand().isSameSuit());
		Assert.assertTrue(testPlayer.hasAce());
		Assert.assertEquals(Player.HandRank.STRAIGHT_FLUSH, testPlayer.getHandRank());
		Assert.assertEquals(testPlayer.getCardHand().getCard(0).getCardRank(), Card.Rank.FIVE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(1).getCardRank(), Card.Rank.FOUR);
		Assert.assertEquals(testPlayer.getCardHand().getCard(2).getCardRank(), Card.Rank.THREE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(3).getCardRank(), Card.Rank.TWO);
		Assert.assertEquals(testPlayer.getCardHand().getCard(4).getCardRank(), Card.Rank.ACE);
	}
	
	/**
	 * Test to see if Straight Flush with low ace is properly identified.
	 * @throws Exception problem with reflection
	 */
	@Test
	public void testNotStraight() throws Exception {
		CardPile deck = new CardPile();
		Player testPlayer;
		
		List<Card> cardPileTest = setCardPilePublic(deck);
		
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.TEN));
		cardPileTest.add(new Card(Card.Suit.CLUB, Card.Rank.NINE));
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.EIGHT));
		cardPileTest.add(new Card(Card.Suit.HEART, Card.Rank.TWO));
		cardPileTest.add(new Card(Card.Suit.SPADE, Card.Rank.ACE));
		
		testPlayer = new Player("test");
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
		testPlayer.getCardHand().draw(deck);
			
		testPlayer.sort();
		System.out.println(testPlayer + testPlayer.printHandRank());
		Assert.assertFalse(testPlayer.getCardHand().isSequence());
		Assert.assertFalse(testPlayer.getCardHand().isSameSuit());
		Assert.assertTrue(testPlayer.hasAce());
		Assert.assertEquals(Player.HandRank.HIGH_CARD, testPlayer.getHandRank());
		Assert.assertEquals(testPlayer.getCardHand().getCard(0).getCardRank(), Card.Rank.ACE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(1).getCardRank(), Card.Rank.TEN);
		Assert.assertEquals(testPlayer.getCardHand().getCard(2).getCardRank(), Card.Rank.NINE);
		Assert.assertEquals(testPlayer.getCardHand().getCard(3).getCardRank(), Card.Rank.EIGHT);
		Assert.assertEquals(testPlayer.getCardHand().getCard(4).getCardRank(), Card.Rank.TWO);
	}
}
